@extends('layouts.app')

@section('title', 'Login')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login</h3>
                </div>
                <div class="panel-body">
                    
                    <h2 class="text-center">{{ config('app.name') }}</h2>

                    <form action="{{ route('login') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="passwordAcademicYearController" class="form-control">
                        </div>

                        <button type="submit" class="btn btn-primary">Login</button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
