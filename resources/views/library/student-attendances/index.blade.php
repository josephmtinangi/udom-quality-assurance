@extends('layouts.secondary')

@section('page_title', 'Library Student Attendances')

@section('main')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Student Library Attendances</h3>
	</div>
	<div class="panel-body">
		
		{{ $studentLibraryAttendances }}

	</div>
</div>

@endsection

@section('aside')

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">#</h3>
		</div>
		<div class="panel-body">
		
			<form action="{{ route('library.student-attendances.store') }}" method="POST">
				{{ csrf_field() }}

				<div class="form-group">
					<label for="registration_number">Registration Number</label>
					<input type="text" name="registration_number" id="registration_number" value="{{ old('registration_number') }}" class="form-control">
				</div>

				<button type="submit" class="btn btn-primary">Submit</button>
			</form>

		</div>
	</div>

@endsection

