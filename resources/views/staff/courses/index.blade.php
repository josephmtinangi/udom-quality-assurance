@extends('layouts.primary')

@section('page_title', 'Courses')

@section('main_title', 'Courses')

@section('main')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Courses</h3>
		</div>
		<div class="panel-body">
				@if($courses->count())
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>SN</th>
						<th>Name</th>
						<th>Code</th>
						<th>Credits</th>
						<th>Details</th>
					</tr>
				</thead>
				<tbody>
					@php $count=1 @endphp
					@foreach($courses as $course)
					<tr>
						<td>{{ $count++ }}</td>
						<td>{{ $course->name }}</td>
						<td>{{ $course->code }}</td>
						<td>{{ $course->credits }}</td>
						<td><a href="{{ route('staff.courses.show', $course) }}">Details</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@else
		<div class="alert alert-info">No courses</div>
	@endif

		</div>
	</div>


@endsection

