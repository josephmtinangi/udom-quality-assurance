@extends('layouts.primary')

@section('page_title', $course->name)

@section('main_title', $course->name)

@section('main')


	<!-- Create Teaching Plan -->
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Create Teaching Plan</h3>
			</div>
			<div class="panel-body">
				
				<form action="{{ route('courses.teaching-plans.store', $course->id) }}" method="POST">
					{{ csrf_field() }}

					<div class="form-group">
						<label for="course_assesment_id">Course Assesment</label>
						<select name="course_assesment_id" id="course_assesment_id" class="form-control">
							@foreach($courseAssements as $courseAssement)
								<option value="{{ $courseAssement->id }}">{{ $courseAssement->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label for="number">Number</label>
						<input type="number" name="number" id="number" class="form-control">
					</div>

					<div class="form-group">
						<label for="">Date</label>
						<input type="date" class="form-control">
					</div>

					<button type="submit" class="btn btn-primary">Add</button>
				</form>

			</div>
		</div>
	<!-- // Create Teaching Plan -->

@endsection


