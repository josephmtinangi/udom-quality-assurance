@extends('layouts.primary')

@section('page_title', $course->name)

@section('main_title', $course->name)

@section('main')

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Course Details and delivery Plan</h3>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th>Name</th>
							<th>Code</th>
							<th>Credits</th>
						</tr>
					</thead>
					<tbody>
							<tr><td>{{$course->name}}</td><td>{{$course->code}}</td><td>{{$course->credits}}</td></tr>
					</tbody>
				</table>
			</div>

			@if($course->teachingPlans()->count())
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>SN</th>
							<th>Name</th>
							<th>Number</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						@foreach($course->teachingPlans as $teachingPlan)
							<tr>
								<td></td>
								<td>{{ $teachingPlan->courseAssesment->name }}</td>
								<td>{{ $teachingPlan->number }}</td>
								<td>
									<a href="#" class="btn btn-info btn-sm">Edit</a>
								</td>
								<td>
									<a href="#" class="btn btn-danger btn-sm">Delete</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@else
				<h4>Instructor Course Delievery Plan</h4>
				<div class="alert alert-info">No course delivery plan</div>
			@endif
				<a href="{{ route('courses.teaching-plans.create', $course->id) }}" class="btn btn-primary">Add</a>
		</div>
	</div>

	<!-- Teaching Plan -->
		<!--div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Teaching Plan</h3>
			</div>
			<div class="panel-body">
				
				@if($course->teachingPlans()->count())
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>SN</th>
									<th>Name</th>
									<th>Number</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								@foreach($course->teachingPlans as $teachingPlan)
									<tr>
										<td></td>
										<td>{{ $teachingPlan->courseAssesment->name }}</td>
										<td>{{ $teachingPlan->number }}</td>
										<td>
											<a href="#" class="btn btn-info btn-sm">Edit</a>
										</td>
										<td>
											<a href="#" class="btn btn-danger btn-sm">Delete</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				@else
					<div class="alert alert-info">No course delivery plan</div>
				@endif

			</div>
			<div class="panel-footer">
				<a href="{{ route('courses.teaching-plans.create', $course->id) }}" class="btn btn-primary">Add</a>
			</div>
		</div-->
	<!-- // Teaching Plan -->

@endsection


