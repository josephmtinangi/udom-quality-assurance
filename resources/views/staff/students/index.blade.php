@extends('layouts.primary')

@section('page_title', 'Students')

@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Students</h3>
        </div>
        <div class="panel-body">
         	@php $count=1 @endphp
            @if($students->count())
            <div class="table-responsive">
            	<table class="table table-bordered">
            		<thead>
            			<tr>
            				<th>SN</th>
            				<th>First Name</th>
            				<th>Last Name</th>
            				<th>Reg#</th>
            				<th>Phone</th>
            				<th>Email</th>
            			</tr>
            		</thead>
            		<tbody>
						@foreach($students as $student)
							<tr>
								<td>{{$count++}}</td>
								<td>{{$student->first_name}}</td>
								<td>{{$student->last_name}}</td>
								<td>{{$student->registration_number}}</td>
								<td>{{$student->phone_number}}</td>
								<td>{{$student->email}}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="alert alert-info">No Student you advise</div>
        @endif

        </div>
    </div>

@endsection

