@extends('layouts.secondary')

@section('page_title', $student->registration_number . ' - ' . $student->name())

@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ $student->registration_number }} - {{ $student->name() }}</h3>
        </div>
        <div class="panel-body">

            {{ $student }}

        </div>
    </div>

@endsection

@section('aside')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Consultation Sessions</h3>
	</div>
	<div class="panel-body">
		
		{{ $student->consultationSessions }}

	</div>
</div>

@endsection

