@extends('layouts.primary')

@section('page_title', 'Colleges')

@section('main')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Colleges</h3>
	</div>
	<div class="panel-body">
	
		@if($colleges->count())
			@php $count=1 @endphp
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>SN</th>
							<th>Name</th>
							<th>Acronym</th>
						</tr>
					</thead>
					<tbody>
						@foreach($colleges as $college)
						<tr>
							<td>{{$count++}}</td>
							<td>{{$college->name}}</td>
							<td>{{$college->acronym}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="alert alert-info">No College found</div>
		@endIf
		
	</div>
</div>

@endsection

