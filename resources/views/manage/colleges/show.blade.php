@extends('layouts.primary')

@section('page_title', $college->name)

@section('main')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">{{ $college->name }}</h3>
	</div>
	<div class="panel-body">
		
		{{ $college }}

	</div>
</div>

@endsection

