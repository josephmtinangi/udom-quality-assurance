@extends('layouts.secondary')

@section('page_title', 'Manage ' . $staff->first_name)

@section('main')

<div class="panel panel-default">
    <div class="panel-heading">Personal Information</div>

    <div class="panel-body">
        
		<dl class="dl-horizontal">
		  <dt>First name</dt>
		  <dd>{{ $staff->first_name }}</dd>
		  <dt>Middle name</dt>
		  <dd>{{ $staff->middle_name }}</dd>
		  <dt>Last name</dt>
		  <dd>{{ $staff->last_name }}</dd>
		  <dt>Phone number</dt>
		  <dd>{{ $staff->phone_number }}</dd>
		  <dt>Email Address</dt>
		  <dd>{{ $staff->email }}</dd>
		  <dt>Department</dt>
		  <dd></dd>
		</dl>

		<div class="text-center">
			<a href="#" class="btn btn-success">Edit</a>		
			<a href="#" class="btn btn-warning">Deactivate</a>		
			<a href="#" class="btn btn-danger">Delete</a>		
		</div>

    </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Courses ({{ $staff->coursesCount() }})</h3>
	</div>
	<div class="panel-body">
		
		@if($staff->coursesCount())
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>SN</th>
							<th>Code</th>
							<th>Name</th>
							<th>Credits</th>
							<th>Assigned</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1 @endphp
						@foreach($staff->courses as $course)
							<tr>
								<td class="text-right">{{ $i++ }}.</td>
								<td>{{ $course->code }}</td>
								<td>{{ $course->name }}</td>
								<td>{{ $course->credits }}</td>
								<td>{{ $course->pivot->created_at }}</td>
								<td>
									<a href="{{ route('manage.staff.courses.show', [$staff->id, $course->id]) }}">Details</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>				
			</div>
		@else
			<div class="alert alert-info">No Course Assigned</div>
		@endif

	</div>
	<div class="panel-footer">
		<a href="{{ route('manage.staff.courses.create', $staff->id) }}" class="btn btn-primary">Assign</a>
	</div>
</div>

@endsection

@section('aside')

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Photo</h3>
    </div>
    <div class="panel-body">
        <img src="#" class="img-responsive" alt="Image">
    </div>
</div>


@endsection

