@extends('layouts.primary')

@section('page_title', 'Manage Staff')

@section('main_title', 'Manage Staff')

@section('main')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Manage Staff</h3>
	</div>
	<div class="panel-body">

@if($staff->count())

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingOne">
	      <h4 class="panel-title">
	        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
	          Search
	        </a>
	      </h4>
	    </div>
	    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
	      <div class="panel-body">
	        
	      </div>
	    </div>
	  </div>
	</div>
	
	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>SN</th>
					<th>First name</th>
					<th>Middle name</th>
					<th>Last name</th>
					<th>Created</th>
					<th>Details</th>
				</tr>
			</thead>
			<tbody>
				@php $count=1 @endphp
				@foreach($staff as $staf)
					<tr>
						<td>{{ $count++ }}</td>
						<td>{{ $staf->first_name }}</td>
						<td>{{ $staf->middle_name }}</td>
						<td>{{ $staf->last_name }}</td>
						<td>{{ $staf->created_at }}</td>
						<td>
							<a href="{{ route('staff.show', $staf->id) }}">Details</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	{{ $staff->links() }}
@else
	<div class="alert alert-info">No Staff</div>
@endif

	</div>
</div>

@endsection

