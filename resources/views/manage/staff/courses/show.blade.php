@extends('layouts.primary')

@section('page_title', $course->code . ' - ' . $course->name)

@section('main')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">{{ $course->code . ' - ' . $course->name }}</h3>
	</div>
	<div class="panel-body">
		
		Course <br>
		{{ $course }}

		<hr>

		Teaching Plans <br>
		{{ $course->teachingPlans }}

		<hr>

		Class attendances <br>
		{{ $course->classAttendances }}

	</div>
</div>

@endsection

