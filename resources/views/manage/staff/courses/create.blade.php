@extends('layouts.primary')

@section('page_title', 'Assign Course to ' . $staff->name())

@section('main')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Assign Course to {{ $staff->name() }}</h3>
	</div>
	<div class="panel-body">
		
		<form action="{{ route('manage.staff.courses.store', $staff->id) }}" method="POST">
			{{ csrf_field() }}

			<div class="form-group">
				<label for="semester_id">Semester</label>
				<select name="semester_id" id="semester_id" class="form-control" required>
					<option value="">-Select-</option>
					@foreach($semesters as $semester)
						<option value="{{ $semester->id }}">{{ $semester->name }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label for="course_id">Course</label>
				<select name="course_id" id="course_id" class="form-control" required>
					<option value="">-Select-</option>
					@foreach($courses as $course)
						<option value="{{ $course->id }}">{{ $course->code }} - {{ $course->name }}</option>
					@endforeach
				</select>
			</div>

			<button type="submit" class="btn btn-primary">Assign</button>
		</form>

	</div>
</div>

@endsection

