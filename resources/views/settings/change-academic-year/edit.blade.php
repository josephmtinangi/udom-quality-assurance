@extends('layouts.primary')

@section('page_title', 'Change Academic Year')

@section('main_title', 'Change Academic Year')

@section('main')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Change Academic Year</h3>
	</div>
	<div class="panel-body">
		<div class="col-sm-6">
			<form action="{{ route('change-academic-year.update') }}" method="POST">
				{{ csrf_field() }}

				<div class="form-group">
					<label for="academic_year">Select Academic Year</label>
					<select name="academic_year" id="academic_year" class="form-control">
						@foreach($academicYears as $academicYear)
							<option value="{{ $academicYear->id }}" {{ $academicYear->id == Session::get('academicYear')->id ? 'selected="selected"' : '' }}>{{ $academicYear->name }}</option>
						@endforeach
					</select>
				</div>

				<button type="submit" class="btn btn-primary">Change</button>

			</form>
		</div>
	</div>
</div>

@endsection

