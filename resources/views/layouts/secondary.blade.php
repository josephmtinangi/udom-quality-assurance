<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('page_title') | {{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
       
        @include('layouts.partials._navbar')
        
        @include('layouts.partials._flash_message')


            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Top bar -->
                        @include('layouts.partials._topbar')
                        <!-- // Top bar -->        
                    </div>        
                </div>
                <div class="row">
                    <div class="col-md-2">
                        @include('layouts.partials._sidebar')
                    </div>
                    <div class="col-md-7">

                        @yield('main')

                    </div>
                    <div class="col-md-3">

                        @yield('aside')

                    </div>
                </div>
            </div>        

        @include('layouts.partials._footer')
        
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
