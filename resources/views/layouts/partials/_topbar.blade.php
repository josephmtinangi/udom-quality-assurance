@if(Auth::check())
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
                <strong>Logged in as:</strong> {{ Auth::user()->username }}
            </div>
            <div class="col-sm-4 hidden-xs">
                <strong>Academic Year: </strong> {{ Session::get('academicYear')->name }}
            </div>
            <div class="col-sm-4 hidden-xs">
                {{ Carbon\Carbon::now()->format('l jS \\of F Y') }}
            </div>
        </div>
    </div>
</div>
@endif