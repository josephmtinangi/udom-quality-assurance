<!-- All -->
All
<div class="list-group">
    <a href="{{ route('home') }}" class="list-group-item active">Home</a>
    <a href="#" class="list-group-item">Item 3</a>
    <a href="{{ route('change-academic-year.edit') }}" class="list-group-item">Change Academic Year</a>
</div>
<!-- // All -->

<!-- Principal of college -->
Principal of college
<div class="list-group">
	<a href="#" class="list-group-item">Item 2</a>
	<a href="#" class="list-group-item">Item 3</a>
</div>
<!-- // Principal of college -->

<!-- Dean of school -->
Dean of school
<div class="list-group">
	<a href="#" class="list-group-item">Item 2</a>
	<a href="#" class="list-group-item">Item 3</a>
</div>
<!-- // Dean of school -->

<!-- Head of Department -->
Head of department
<div class="list-group">
    <a href="{{ route('staff.index') }}" class="list-group-item">Staff</a>
	<a href="#" class="list-group-item">Item 3</a>
</div>
<!-- // Head of Department -->

<!-- Staff -->
Staff
<div class="list-group">
	<a href="{{ route('staff.courses.index') }}" class="list-group-item">Courses</a>
	<a href="{{ route('staff.students.index') }}" class="list-group-item">Students</a>
	<a href="#" class="list-group-item">Item 3</a>
</div>
<!-- // Staff -->

<!-- Library -->
Library
<div class="list-group">
	<a href="{{ route('library.student-attendances.index') }}" class="list-group-item">Student Attendance</a>
	<a href="#" class="list-group-item">Item 2</a>
	<a href="#" class="list-group-item">Item 3</a>
</div>
<!-- // Library -->

<!-- Class representative -->
Class representative
<div class="list-group">
	<a href="#" class="list-group-item">Item 2</a>
	<a href="#" class="list-group-item">Item 3</a>
</div>
<!-- // Class representative -->

<!-- Student -->
Student
<div class="list-group">
	<a href="#" class="list-group-item">Item 2</a>
	<a href="#" class="list-group-item">Item 3</a>
</div>
<!-- // Student -->

<!-- Manage -->
Manage
<div class="list-group">
	<a href="{{ route('colleges.index') }}" class="list-group-item">Colleges</a>
	<a href="#" class="list-group-item">Item 3</a>
</div>
<!-- // Manage -->
