<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['namespace' => 'Manage', 'middleware' => 'auth', 'prefix' => 'manage'], function () {
	Route::resource('staff', 'StaffController');

	Route::get('staff/{staff}/courses', 'Staff\CourseController@index')->name('manage.staff.courses.index');
	Route::get('staff/{staff}/courses/create', 'Staff\CourseController@create')->name('manage.staff.courses.create');
	Route::get('staff/{staff}/courses/{course}', 'Staff\CourseController@show')->name('manage.staff.courses.show');
	Route::post('staff/{staff}/courses', 'Staff\CourseController@store')->name('manage.staff.courses.store');

	Route::resource('colleges', 'CollegeController');
});

Route::group(['namespace' => 'Settings', 'middleware' => 'auth'], function () {
	Route::get('settings/change-academic-year', 'AcademicYearController@edit')->name('change-academic-year.edit');
	Route::post('settings/change-academic-year', 'AcademicYearController@update')->name('change-academic-year.update');
});

Route::group(['prefix' => 'staff-area', 'namespace' => 'Staff', 'middleware' => 'auth'], function () {
	Route::get('courses', 'CourseController@index')->name('staff.courses.index');
	Route::get('courses/{course}', 'CourseController@show')->name('staff.courses.show');
	
	Route::resource('courses.teaching-plans', 'TeachingPlanController');

	Route::get('students', 'StudentController@index')->name('staff.students.index');
	Route::get('students/{student}', 'StudentController@show')->name('staff.students.show');
});

// Library
Route::group(['prefix' => 'library', 'namespace' => 'Library', 'middleware' => 'auth'], function () {
	Route::get('student-attendances', 'StudentAttendaceController@index')->name('library.student-attendances.index');
	Route::post('student-attendances', 'StudentAttendaceController@store')->name('library.student-attendances.store');
});

