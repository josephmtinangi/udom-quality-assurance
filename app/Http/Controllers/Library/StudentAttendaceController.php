<?php

namespace App\Http\Controllers\Library;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StudentLibraryAttendance;

class StudentAttendaceController extends Controller
{
    public function index()
    {
    	$studentLibraryAttendances = auth()->user()->staff->libraries()->with('students')->first()->students;

    	return view('library.student-attendances.index', compact('studentLibraryAttendances'));
    }

    public function store(Request $request)
    {
    	$student = Student::whereRegistrationNumber($request->registration_number)->first();

    	if(!$student)
    	{
    		notify()->flash('There is no student with this registration number!', 'danger');

    		return back();
    	}

    	StudentLibraryAttendance::create([
    		'academic_year_id' => \Session::get('academicYear')->id,
    		'semester_id' => \App\Models\Semester::first()->id,
    		'library_id' => \App\Models\Library::first()->id,
    		'staff_id' => auth()->user()->staff->id,
    		'student_id' => $student->id,
    	]);

    	notify()->flash('Success!', 'success');

    	return back();
    }
}
