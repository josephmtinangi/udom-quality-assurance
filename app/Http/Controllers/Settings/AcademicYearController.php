<?php

namespace App\Http\Controllers\Settings;

use App\Models\AcademicYear;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AcademicYearController extends Controller
{
    public function edit()
    {
        $academicYears = AcademicYear::latest()->get();
        $sessionAcademicYear = Session::get('academicYear');
        return view('settings.change-academic-year.edit', compact('academicYears', 'sessionAcademicYear'));
    }

    public function update(Request $request)
    {
        $academicYear = AcademicYear::findOrFail($request->academic_year);

        Session::put('academicYear', $academicYear);

        notify()->flash('Academic Year Changed successful!', 'success');

        return back();
    }
}
