<?php

namespace App\Http\Controllers\Manage\Staff;

use App\Models\Semester;
use App\Models\Staff;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class CourseController extends Controller
{
    public function index(Staff $staff)
    {
        return $staff->courses;
    }

    public function show(Staff $staff, $id)
    {
        $course = Course::with(['classAttendances', 'teachingPlans'])->find($id);

        return view('manage.staff.courses.show', compact('staff', 'course'));
    }

    public function create(Staff $staff)
    {
    	$courses = Course::latest()->get();
    	$semesters = Semester::get();
    	return view('manage.staff.courses.create', compact('staff', 'courses','semesters'));
    }

    public function store(Request $request, Staff $staff)
    {
    	
    	$this->validate($request, [
    		'course_id' => 'required'
    	]);

    	$course = Course::findOrFail($request->course_id);

    	$staff->courses()->attach($course, [
    		'academic_year_id' => Session::get('academicYear')->id,
            'semester_id' => $request->semester_id,
    	]);

    	notify()->flash('Course assigned successful!', 'success');

    	return redirect()->route('staff.show', $staff->id);
    }
}
