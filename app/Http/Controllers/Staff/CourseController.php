<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CourseController extends Controller
{
    public function index()
    {
    	$courses = auth()->user()->staff->getCoursesInActiveAcademicYear()->get();
    	return view('staff.courses.index', compact('courses'));
    }

    public function show($id)
    {
    	$course = auth()->user()->staff->getCoursesInActiveAcademicYear()->findOrFail($id);
    	return view('staff.courses.show', compact('course'));
    }
}
