<?php

namespace App\Http\Controllers\Staff;

use App\Models\CourseAssesment;
use App\Models\TeachingPlan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class TeachingPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $courseAssements = CourseAssesment::get();
        $course = auth()->user()->staff->getCoursesInActiveAcademicYear()->findOrFail($id);
        return view('staff.courses.teaching-plans.create', compact('courseAssements','course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $course = auth()->user()->staff->getCoursesInActiveAcademicYear()->findOrFail($id);

        $teachingPlan = new TeachingPlan;
        $teachingPlan->course_assesment_id = $request->course_assesment_id;
        $teachingPlan->number = $request->number;
        $teachingPlan->academic_year_id = Session::get('academicYear')->id;
        $teachingPlan->semester_id = $course->pivot->semester_id;
        $teachingPlan->staff_id = auth()->user()->staff->id;
        $teachingPlan->course_id = $course->id;
        $teachingPlan->date_range = Carbon::now()->addWeeks(5);
        $teachingPlan->save();

        notify()->flash('Teaching plan added successful!', 'success');

        return redirect()->route('staff.courses.show', $course);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
