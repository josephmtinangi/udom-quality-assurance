<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function index()
    {
    	$students = auth()->user()->staff->students;
    	return view('staff.students.index', compact('students'));
    }

    public function show($id)
    {
    	$student = auth()->user()->staff->students()->with('consultationSessions')->findOrFail($id);
    	return view('staff.students.show', compact('student'));	
    }
}
