<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeachingPlan extends Model
{
    public function courseAssesment()
    {
        return $this->belongsTo(CourseAssesment::class);
    }
}
