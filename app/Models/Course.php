<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function teachingPlans()
    {
        return $this->hasMany(TeachingPlan::class);
    }

    public function classAttendances()
    {
        return $this->hasMany(StaffClassAttendance::class);
    }
}
