<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    public function students()
    {
    	return $this->belongsToMany(Student::class, 'student_library_attendances');
    }
}
