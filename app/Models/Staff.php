<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Staff extends Model
{

    public function name()
    {
        return $this->title . ' ' . $this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name;
    }
    
    public function courses()
    {
        return $this->belongsToMany(Course::class)
            ->withTimestamps()
            ->withPivot(['academic_year_id', 'semester_id']);
    }

    public function coursesCount()
    {
        return $this->courses()->whereAcademicYearId(Session::get('academicYear')->id)->count();
    }

    public function getCoursesInActiveAcademicYear()
    {
        return $this->courses()->where('academic_year_id', Session::get('academicYear')->id);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'academic_advisors')
            ->withTimestamps()
            ->withPivot(['academic_year_id', 'semester_id']);
    }

    public function libraries()
    {
        return $this->belongsToMany(Library::class);
    }
}
