<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentLibraryAttendance extends Model
{
    protected $fillable = [
    	'academic_year_id',
    	'semester_id',
    	'library_id',
    	'staff_id',
    	'student_id'
    ];
}
