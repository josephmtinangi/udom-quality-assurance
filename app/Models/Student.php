<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function name()
    {
        return $this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name;
    }

    public function consultationSessions()
    {
        return $this->hasMany(StudentAcademicAdvisorsConsultation::class);
    }
}
