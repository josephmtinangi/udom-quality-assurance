<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registration_number')->unique();
            $table->string('form4_index_no')->unique()->nullable();
            $table->string('form6_index_no')->unique()->nullable();
            $table->date('admission_date');
            $table->date('birth_date')->nullable();
            $table->string('birth_place')->nullable();
            $table->string('place_of_residence')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('email_address')->unique()->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('sponsorship')->nullable();
            $table->string('citizenship')->nullable();
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->integer('program_id')->unsigned()->index()->nullable();
            $table->timestamps();
        });

        Schema::table('students', function($table){
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('program_id')
                    ->references('id')->on('programs')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
