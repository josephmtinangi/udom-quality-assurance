<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group_name');
            $table->timestamps();
        });

        Schema::table('groups', function($table){
          /*  $table->integer('staff_id')->unsigned()->index()->nullable();
            $table->foreign('staff_id')
                    ->references('id')->on('staff')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');*/
            
            $table->integer('user_id')->unsigned()->index()->nullable();
                $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

         App\Group::create([
            'group_name' => 'University R/W',
            'user_id' => App\User::find(1)->id,
        ]);
         App\Group::create([
            'group_name' => 'University R',
            'user_id' => App\User::find(1)->id,
        ]);
         App\Group::create([
            'group_name' => 'College R/W',
            'user_id' => App\User::find(1)->id,
        ]);
         App\Group::create([
            'group_name' => 'College R',
            'user_id' => App\User::find(1)->id,
        ]);
         App\Group::create([
            'group_name' => 'School R/W',
            'user_id' => App\User::find(1)->id,
        ]);
         App\Group::create([
            'group_name' => 'School R',
            'user_id' => App\User::find(1)->id,
        ]);
         App\Group::create([
            'group_name' => 'Department R/W',
            'user_id' => App\User::find(1)->id,
        ]);
         App\Group::create([
            'group_name' => 'Department R',
            'user_id' => App\User::find(1)->id,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('levels', function (Blueprint $table) {
            //
        });
    }
}
