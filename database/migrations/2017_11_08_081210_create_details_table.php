<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('surname', 50)->nullable();
            $table->char('gender', 1);
            $table->string('image_url')->default('default.jpg');
            $table->timestamps();
            // $table->integer('address_id')->unsigned()->index()->nullable();
        });

        Schema::table('details', function($table){
            /*$table->foreign('address_id')
                    ->references('id')->on('addresses')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');*/
        });

         App\Detail::create([
            'first_name' => 'Nickamen',
            'last_name' => 'Thomson',
            'surname' => 'Sambo',
            'gender' => 'M',
            // 'address_id' => App\Address::find(1)->id,
        ]);
  
}
