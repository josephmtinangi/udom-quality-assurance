<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaboratoryStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laboratory_staff', function (Blueprint $table) {
            $table->unsignedInteger('academic_year_id');
            $table->unsignedInteger('semester_id');            
            $table->unsignedInteger('laboratory_id');
            $table->unsignedInteger('staff_id');
            $table->timestamps();

            $table->foreign('academic_year_id')->references('id')->on('academic_years')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('semester_id')->references('id')->on('semesters')->onUpdate('cascade')->onDelete('cascade');            
            $table->foreign('laboratory_id')->references('id')->on('laboratories')->onUpdate('cascade')->onDelete('cascade');            
            $table->foreign('staff_id')->references('id')->on('staff')->onUpdate('cascade')->onDelete('cascade'); 

            $table->primary(['academic_year_id', 'semester_id', 'laboratory_id', 'staff_id'], 'acy_semid_lab_id_stfid');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laboratory_staff');
    }
}
