<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->username,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Staff::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->title,
        'first_name' => $faker->firstName,
        'middle_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone_number' => $faker->phoneNumber,
        'email' => $faker->safeEmail,
        'user_id' => function () {
            return factory('App\Models\User')->create()->id;
        }
    ];
});

$factory->define(App\Models\Student::class, function (Faker\Generator $faker) {

    return [
        'registration_number' => 'T/UDOM/20' . $faker->randomDigit . '/' . $faker->randomNumber(4, true),
        'first_name' => $faker->firstName,
        'middle_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone_number' => $faker->phoneNumber,
        'email' => $faker->safeEmail,
        'user_id' => function () {
            return factory('App\Models\User')->create()->id;
        }
    ];
});
