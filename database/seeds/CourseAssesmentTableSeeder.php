<?php

use App\Models\CourseAssesment;
use Illuminate\Database\Seeder;

class CourseAssesmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseAssesment::create(['name' => 'Quiz']);
        CourseAssesment::create(['name' => 'Assignment']);
        CourseAssesment::create(['name' => 'Test']);
    }
}
