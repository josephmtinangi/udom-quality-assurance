<?php

use App\Models\Program;
use App\Models\ProgramType;
use Illuminate\Database\Seeder;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programs')->delete();

        Program::create([
        	'name' => 'Bachelor Of Science In Business Information Systems',
        	'acronym' => 'BSc. BIS',
        	'code' => 'BSc. BIS',
        	'duration' => 3,
        	'program_type_id' => ProgramType::where('name', 'Bachelor')->first()->id,
        ]);
        Program::create([
            'name'=>'Bachelor of Science in Petroleum Engineering',
            'acronym'=>'BSc. PE',
            'code'=>'BSc. PE',
            'duration'=>4,
            'program_type_id'=>ProgramType::where('name','Bachelor')->first()->id,
        ]);
        Program::create([
            'name'=>'Bachelor of Science in Mining Engineering',
            'acronym'=>'BSc (ME)',
            'code'=>'BSc (ME)',
            'duration'=>4,
            'program_type_id'=>ProgramType::where('name','Bachelor')->first()->id,
        ]);
        Program::create([
            'name'=>'Bachelor of Science in Applied Geology',
            'acronym'=>'BSc (AG)',
            'code'=>'BSc (AG)',
            'duration'=>3,
            'program_type_id'=>ProgramType::where('name','Bachelor')->first()->id,
        ]);
        Program::create([
            'name'=>'Bachelor of Science in Geoinformatics',
            'acronym'=>'BSc (Geoinfo)',
            'code'=>'BSc (Geoinfo)',
            'duration'=>3,
            'program_type_id'=>ProgramType::where('name','Bachelor')->first()->id,
        ]);
        Program::create([
            'name'=>'Bachelor of Science in Hydrogeology',
            'acronym'=>'BSc (Hydrogeo)',
            'code'=>'BSc (Hydrogeo)',
            'duration'=>3,
            'program_type_id'=>ProgramType::where('name','Bachelor')->first()->id,
        ]);
        Program::create([
            'name'=>'Bachelor of Science in Metallurgy and Mineral Processing Engineering',
            'acronym'=>' BSc (MMP)',
            'code'=>' BSc (MMP)',
            'duration'=>4,
            'program_type_id'=>ProgramType::where('name','Bachelor')->first()->id,
        ]);
    }
}
