<?php

use App\Models\ProgramType;
use Illuminate\Database\Seeder;

class ProgramTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('program_types')->delete();

        ProgramType::create([
        	'name' => 'Certificate',
        ]);
        ProgramType::create([
        	'name' => 'Diploma',
        ]);
        ProgramType::create([
        	'name' => 'Bachelor',
        ]);
        ProgramType::create([
        	'name' => 'Masters',
        ]);
        ProgramType::create([
        	'name' => 'PhD',
        ]);
    }
}
