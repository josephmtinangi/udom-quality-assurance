<?php
use App\Models\Department;
use App\Models\School;
use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->delete();

        Department::create([
        	'name' => 'Compueter Science',
        	'acronym' => 'Dept Cs',
        	'school_id' => School::where('name', 'School of Informatics')->first()->id,
        ]);

    }
}
