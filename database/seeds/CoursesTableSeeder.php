<?php

use App\Models\Course;
use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('courses')->delete();

        Course::create([
        	'name' => 'Communication Skills For Scientists',
        	'code' => 'LG 103',
        	'credits' => 10,
        ]);

        Course::create([
        	'name' => 'Introduction To High Level Programming',
        	'code' => 'CS 110',
        	'credits' => 10,
        ]);
    }
}
