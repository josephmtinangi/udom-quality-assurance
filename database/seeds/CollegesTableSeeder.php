<?php

use App\Models\College;
use Illuminate\Database\Seeder;

class CollegesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colleges')->delete();

        College::create([
        	'name' => 'College Informatics and Virtual Education',
        	'acronym' => 'CIVE',
        ]);

        College::create([
        	'name' => 'College of Natural & Mathematical Sciences',
        	'acronym' => 'CNMS',
        ]);

        College::create([
        	'name' => 'College of Earth Sciences',
        	'acronym' => 'COES',
        ]);

        College::create([
        	'name' => 'College of Health & Allied Sciences',
        	'acronym' => 'COHAS',
        ]);

        College::create([
        	'name' => 'College of Education',
        	'acronym' => 'COED',
        ]);

        College::create([
        	'name' => 'College of humanities & Social Sciences',
        	'acronym' => 'CHSS',
        ]);
    }
}
