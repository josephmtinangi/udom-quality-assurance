<?php

use App\Models\Library;
use App\Models\College;
use Illuminate\Database\Seeder;

class LibrariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('libraries')->delete();

        Library::create([
        	'name' => 'CIVE Library',
        	'college_id' => College::whereAcronym('CIVE')->first()->id,
        ]);
    }
}
