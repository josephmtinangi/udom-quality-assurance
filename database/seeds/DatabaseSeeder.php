<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(CollegesTableSeeder::class);
        $this->call(SchoolsTableSeeder::class);
     
        $this->call(ProgramTypesTableSeeder::class);
        $this->call(ProgramsTableSeeder::class);
        
        $this->call(CoursesTableSeeder::class);
        
        $this->call(AcademicYearsTableSeeder::class);
        $this->call(SemestersTableSeeder::class);
        
        $this->call(StaffTableSeeder::class);
    }
}
