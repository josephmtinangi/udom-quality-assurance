<?php

use App\Models\AcademicYear;
use Illuminate\Database\Seeder;

class AcademicYearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('academic_years')->delete();

        AcademicYear::create([
            'name' => '2013/2014',
        ]);

        AcademicYear::create([
            'name' => '2014/2015',
        ]);

        AcademicYear::create([
            'name' => '2015/2016',
        ]);

        AcademicYear::create([
            'name' => '2016/2017',
        ]);

        AcademicYear::create([
        	'name' => '2017/2018',
            'active' => true,
        ]);
    }
}
