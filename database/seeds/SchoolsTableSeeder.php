<?php

use App\Models\School;
use App\Models\College;
use Illuminate\Database\Seeder;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')->delete();

        // CIVE
        School::create([
        	'name' => 'School of Informatics',
        	'acronym' => 'SOI',
        	'college_id' => College::whereAcronym('CIVE')->first()->id,
        ]);
        School::create([
        	'name' => 'School of Virtual Education',
        	'acronym' => 'SOVE',
        	'college_id' => College::whereAcronym('CIVE')->first()->id,
        ]);

        // CNMS
        School::create([
        	'name' => 'School of Physical Sciences',
        	'acronym' => 'SOPS',
        	'college_id' => College::whereAcronym('CNMS')->first()->id,
        ]);
        School::create([
        	'name' => 'School of Mathematical Sciences',
        	'acronym' => 'SOMS',
        	'college_id' => College::whereAcronym('CNMS')->first()->id,
        ]);
        School::create([
        	'name' => 'School of Biological Sciences',
        	'acronym' => 'SOBS',
        	'college_id' => College::whereAcronym('CNMS')->first()->id,
        ]);

        // COES
        School::create([
        	'name' => 'School of Mines & Petroleum Engineering',
        	'acronym' => 'SOMPE',
        	'college_id' => College::whereAcronym('COES')->first()->id,
        ]);

        // COHAS
        School::create([
        	'name' => 'School of Medicine & Dentistry',
        	'acronym' => 'SOMD',
        	'college_id' => College::whereAcronym('COHAS')->first()->id,
        ]);
        School::create([
        	'name' => 'School of Nursing & Public Health',
        	'acronym' => 'SONPH',
        	'college_id' => College::whereAcronym('COHAS')->first()->id,
        ]);

        // COED
        School::create([
        	'name' => 'School of Educational Studies',
        	'acronym' => 'SOES',
        	'college_id' => College::whereAcronym('COED')->first()->id,
        ]);
        School::create([
        	'name' => 'School of Curriculum & Teacher Education',
        	'acronym' => 'SOCTE',
        	'college_id' => College::whereAcronym('COED')->first()->id,
        ]); 

        // CHSS
        School::create([
        	'name' => 'School of Humanities',
        	'acronym' => 'SOH',
        	'college_id' => College::whereAcronym('CHSS')->first()->id,
        ]);
        School::create([
        	'name' => 'School of Social Sciences',
        	'acronym' => 'SOSS',
        	'college_id' => College::whereAcronym('CHSS')->first()->id,
        ]);
        School::create([
        	'name' => 'School of Business Studies & Economics',
        	'acronym' => 'SOBSE',
        	'college_id' => College::whereAcronym('CHSS')->first()->id,
        ]);                       
    }
}
