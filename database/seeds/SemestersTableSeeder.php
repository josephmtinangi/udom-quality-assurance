<?php

use App\Models\Semester;
use Illuminate\Database\Seeder;

class SemestersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('semesters')->delete();

        Semester::create([
        	'name' => 'I',
        ]);

        Semester::create([
        	'name' => 'II',
        ]);
    }
}
