# UDOM Quality Assurance

Demo

[http://udom-qa.herokuapp.com](http://udom-qa.herokuapp.com/)

## Table of contents

- [Quick start](#quick-start)

- [Bugs and feature requests](#bugs-and-feature-requests)

- [Documentation](#documentation)

- [Contributing](#contributing)

- [Versioning](#versioning)

- [Creators](#creators)

- [Copyright and License](#copyright-and-license)

## Quick start

### Installation

#### Server requirements

- PHP >= 5.6.4

- OpenSSL PHP Extension

- PDO PHP Extension

- Mbstring PHP Extension

- Tokenizer PHP Extension

- XML PHP Extension

- [Composer](https://getcomposer.org)

- [NodeJS](https://nodejs.org)

- MySQL or Postgres database

#### How to install

- Clone the repository 

	`git clone https://josephmtinangi@bitbucket.org/josephmtinangi/udom-quality-assurance.git`

- Change directory `cd udom-quality-assurance`

- Install composer dependencies `composer install`

- Install NodeJS dependencies `npm install`

- Copy `.env.example` to `.env` `cp .env.example .env` and change `.env` file according to your server configuration

- Generate key `php artisan key:generate`

- Migrate and seed the database `php artisan migrate --seed`

- You are done

## Bugs and feature requests

Have a bug or a feature request?, [please open a new issue](https://bitbucket.org/josephmtinangi/udom-quality-assurance/issues/new)

## Documentation

> This is a work in progress

## Contributing

> This is a work in progress

## Versioning

This project is maintained under [the Semantic Versioning guidelines](http://semver.org/). Link to the releases wil be provided soon, come here often.

## Creators

- Nickamen Thomson (<nickamenthomson@gmail.com>)

- Idrisa Kipanga (<hamisiidrisa5@gmail.com>)

- Joseph Mtinangi (<josephmtinangi@gmail.com>)


## Copyright and License

> In progress
